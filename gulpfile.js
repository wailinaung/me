const gulp = require('gulp');
const clean = require('gulp-clean');
const plumber = require('gulp-plumber');
const browserSync = require('browser-sync').create();

const eslint = require('gulp-eslint');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');

const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('destroy', () => {
	gulp.src('./public', { read: false, })
		.pipe(clean());
});

gulp.task('server', ['stylesheets'], () => {
	browserSync.init({
		server: {
			proxy: 'wailinag.me.dev',
			baseDir: './public/',
		}
	});
});

gulp.task('assets', () => {
	gulp.src('./src/assets/')
		.pipe(gulp.dest('./public/'));
});

gulp.task('lint', () => {
	gulp.src('./src/js/**/*.js')
		.pipe(plumber())
		.pipe(eslint())
		.pipe(eslint.format())
		.pipe(eslint.failAfterError());
});

gulp.task('scripts', () => {
	gulp.src('./src/js/**/*.js')
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(concat('app.js'))
		.pipe(uglify())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./public/js/'));
});

gulp.task('stylesheets', () => {
	gulp.src('./src/scss/**/*.scss')
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(autoprefixer())
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./public/css/'))
		.pipe(browserSync.stream());
});

gulp.task('markups', () => {
	gulp.src('./src/**/*.html')
		.pipe(gulp.dest('./public/'));
});

gulp.task('build', ['markups', 'stylesheets', 'lint', 'scripts', 'assets']);

gulp.task('watch', () => {
	gulp.watch('./src/scss/**/*.scss', ['stylesheets']);
	gulp.watch('./src/js/**/*.js', ['lint', 'scripts']);
	gulp.watch(['./public/**/*.html', './public/js/**/*.js']).on('change', browserSync.reload);
});

gulp.task('default', ['markups', 'stylesheets', 'lint', 'scripts', 'assets', 'server', 'watch']);
































